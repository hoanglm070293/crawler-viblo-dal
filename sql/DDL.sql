GRANT ALL ON `crawlviblo`.* TO 'crawlerviblo'@'localhost' IDENTIFIED BY 'crawlerviblo@123';
DROP DATABASE IF EXISTS crawlerviblo;
CREATE DATABASE crawlerviblo;
GRANT ALL ON crawlerviblo.* TO crawlerviblo@localhost;

USE crawlerviblo;

CREATE TABLE IF NOT EXISTS `crawlerviblo`.`content` (
  `id`           INT(11)               AUTO_INCREMENT,
  `content`      TEXT                  CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_date` DATETIME     NULL     DEFAULT CURRENT_TIMESTAMP,
  `updated_date` DATETIME     NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  AUTO_INCREMENT = 1 CHARACTER SET utf8 COLLATE utf8_unicode_ci;