package hoanglm.test.framgia.crawler.viblo.dal.configs;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by hoanglm on 11/6/18.
 */
public class JodaConfig {

    public static final String TYPE_TIME = "org.jadira.usertype.dateandtime.joda.PersistentDateTime";
    public static final String TIME_SIDE = "databaseZone";
    public static final String TIME_SIDE_VALUE = "jvm";

    public static DateTime jodaConvert(String datetime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm:ss");
        DateTime dt = formatter.parseDateTime(datetime);
        return dt;
    }
}
