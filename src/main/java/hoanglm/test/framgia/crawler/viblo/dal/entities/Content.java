package hoanglm.test.framgia.crawler.viblo.dal.entities;

import hoanglm.test.framgia.crawler.viblo.dal.configs.JodaConfig;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import javax.persistence.*;

/**
 * Created by hoanglm on 11/6/18.
 */
@Entity
@Table(name = "content", schema = "crawlviblo", catalog = "")
@ToString
@DynamicUpdate
public class Content {

    private Integer id;
    private String content;
    private DateTime createdDate;
    private DateTime updatedDate;

    @PrePersist
    void prePersist() {
        DateTime currentDateTime = new DateTime();
        this.createdDate = currentDateTime;
        this.updatedDate = currentDateTime;
    }

    @PreUpdate
    void preUpdate() {
        this.updatedDate = new DateTime();
    }

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "content", nullable = true)
    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "created_date", nullable = true)
    @Type(type = JodaConfig.TYPE_TIME, parameters = {@org.hibernate.annotations.Parameter(name = JodaConfig.TIME_SIDE, value = JodaConfig.TIME_SIDE_VALUE)})
    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "updated_date", nullable = true)
    @Type(type = JodaConfig.TYPE_TIME, parameters = {@org.hibernate.annotations.Parameter(name = JodaConfig.TIME_SIDE, value = JodaConfig.TIME_SIDE_VALUE)})
    public DateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(DateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Content content = (Content) o;

        if (id != null ? !id.equals(content.id) : content.id != null) return false;
        if (content != null ? !content.equals(content.content) : content.content != null) return false;
        if (createdDate != null ? !createdDate.equals(content.createdDate) : content.createdDate != null) return false;
        if (updatedDate != null ? !updatedDate.equals(content.updatedDate) : content.updatedDate != null) return false;

        return true;
    }
}
