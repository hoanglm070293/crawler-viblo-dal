package hoanglm.test.framgia.crawler.viblo.dal.repositories;

import hoanglm.test.framgia.crawler.viblo.dal.entities.Content;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by hoanglm on 11/6/18.
 */
@Repository
public interface ContentRepository extends PagingAndSortingRepository<Content, String> {
}
