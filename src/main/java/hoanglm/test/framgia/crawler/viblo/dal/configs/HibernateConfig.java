package hoanglm.test.framgia.crawler.viblo.dal.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import java.util.Properties;

/**
 * Created by hoanglm on 11/6/18.
 */
@Configuration
@ComponentScan(basePackages = "hoanglm.test.framgia.crawler.viblo.dal")
@EnableJpaRepositories("hoanglm.test.framgia.crawler.viblo.dal.repositories")
@PropertySource("classpath:application-production.properties")
@EnableTransactionManagement
public class HibernateConfig {
    @PostConstruct
    public void init() {
        System.out.println("Startup");
    }

    @Autowired
    private Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setPackagesToScan(env.getProperty("hibernate.packagesToScan"));
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(getProperties());
        return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
        properties.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        properties.setProperty("hibernate.connection.provider_class", env.getProperty("hibernate.driver_class"));
        properties.setProperty("hibernate.hikari.dataSource.url", env.getProperty("hibernate.db_url"));
        properties.setProperty("hibernate.hikari.dataSourceClassName", env.getProperty("hibernate.data_source"));
        properties.setProperty("hibernate.hikari.dataSource.user", env.getProperty("hibernate.db_username"));
        properties.setProperty("hibernate.hikari.dataSource.password", env.getProperty("hibernate.db_password"));
        properties.setProperty("hibernate.hbm2ddl.import_files_sql_extractor", env.getProperty("hibernate.hbm2ddl.import_files_sql_extractor"));
        properties.setProperty("hibernate.enable_lazy_load_no_trans", env.getProperty("hibernate.enable_lazy_load_no_trans"));
        return properties;
    }
}

